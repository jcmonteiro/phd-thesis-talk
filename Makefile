TEX     = pdflatex
BIBTEX  = bibtex

TEXFLAGS    = -halt-on-error -interaction=batchmode
BIBTEXFLAGS =

_rm = rm -f

TEXFILE = presentation
FIGDIR = img
OTHER_FILES := $(shell find `pwd` -maxdepth 1 -name '*.tex')
STYLE_FILES := $(shell find `pwd` -maxdepth 1 -name '*.sty')
PICTURES := $(shell find `pwd`/$(FIGDIR) -name '*.pdf')

default:
	@echo "\n============== MAIN FILE =============="
	$(MAKE) $(TEXFILE).pdf
	@echo "=======================================\n"

.SUFFIXES: .tex

$(TEXFILE).pdf: $(TEXFILE).tex $(OTHER_FILES) $(PICTURES) $(STYLE_FILES)
	#$(TEX) $(TEXFLAGS) -draftmode $<
	$(eval name := $(basename $<))
	#if grep -s "There were undefined references" $(name).log; then \
	  #$(BIBTEX) $(BIBTEXFLAGS) $(name); \
	#fi
	$(TEX) $(TEXFLAGS) --synctex=1 $<
	count=1 ;\
	while grep -s "Rerun to get cross-references right" $(name).log && [ $$count -le 3 ] ; do \
	  $(TEX) $(TEXFLAGS) --synctex=1 $< ;\
	  count=`expr $$count + 1` ;\
	done
	@echo ""

preamble:
	pdftex -ini -jobname="compiled_preamble" "&pdflatex" mylatexformat.ltx $(TEXFILE).tex

.PHONY: clean 

clean:
	$(_rm) *.aux *.idx *.blg *.bbl *.glo *.bz2 *.toc *.lof *.lot \
	       *.syx *.abx *.lab *.ilg *.los *.ind *.gls *.out *.log *~

remake:
	$(MAKE) clean
	$(MAKE) preamble
	$(MAKE) default