\section*{Literature Review}

\begin{frame}{Literature Review}
\begin{textblock}{0.9}(0.05,0.3)
    The literature review is divided in two areas, which, in the context of
    output tracking with unknown control direction, are intimately related:
    \begin{itemize}
        \item Sliding-mode control (SMC)
        \item \alert<2->{Extremum-seeking control (ESC)}
    \end{itemize}
    \visible<2->{%
        Significant attention is given to ESC since SMC laws for systems with
        unknown control direction are contained in this area.
    }

    \visible<3->{%
        \begin{center}
        \begin{tikzpicture}
            \node [comment box fit]
                {
                    $k_p(x) = x(t) - a$
                };
        \end{tikzpicture}
        \end{center}
    }
\end{textblock}
\end{frame}

%%%%

\begin{frame}{Literature Review \,---\, Sliding-Mode Control (SMC)}
    \begin{tikzpicture}[remember picture, overlay, anchor=north, align=center]
        \coordinate [yshift=-6em] (top) at (current page.north);
        \node [text width=\textwidth] (A1) at (top)
            {The literature of SMC can be divided in two main categories};
        \path (A1) -- ++(0,-2cm) coordinate [pos=0.5] (AB) --
            ++(-0.5\paperwidth,0) -- ++(\paperwidth,0)
            coordinate [pos=0.25, xshift=(\paperwidth-\textwidth)/12] (B1 coord)
            coordinate [pos=0.75, xshift=-(\paperwidth-\textwidth)/12] (B2 coord);
        \node [line width=0.6, rounded corners=4, text width=0.5\textwidth, draw=black] (B1) at (B1 coord)
            {First-Order SMC\\[-1cm]\[\text{sliding} \implies \sigma = 0\]};
        \node [line width=0.6, rounded corners=4, text width=0.5\textwidth, draw=black] (B2) at (B2 coord)
            {Higher-Order SMC\\[-1cm]\[\text{sliding} \implies \sigma = \hdots = \sigma^{(r-1)} = 0\]};
        \draw [->, rounded corners=10, line width=1pt] (A1) -- (AB) -| (B1);
        \draw [->, rounded corners=10, line width=1pt] (A1) -- (AB) -| (B2);
        \draw [line width=1pt] (A1.south) +(-0.25,0) -- +(0.25,0);
    \end{tikzpicture}
\end{frame}

%%%%

\begin{frame}{Literature Review \,---\, First-Order SMC}
\begin{tikzpicture}[remember picture, overlay]
    \node [yshift=-0.4cm] at (current page.center)
        {\includegraphics[scale=0.59]{img/latex/bd_review_fosmc}};
\end{tikzpicture}
\end{frame}

%%%%

\begin{frame}{Literature Review \,---\, Higher-Order SMC}
\begin{tikzpicture}[remember picture, overlay]
    \node [yshift=-0.4cm] at (current page.center)
        {\includegraphics[scale=0.59]{img/latex/bd_review_hosmc}};
\end{tikzpicture}
\end{frame}

%%%%

\begin{frame}{Literature Review \,---\, Extremum-Seeking Control (ESC)}
\begin{textblock}{0.9}(0.05,0.24)
    Extremum-seeking control (ESC) is the name given for a wide variety of
    techniques used to find a system optimum operating condition in real-time.
    There are four classes of ESC strategies:
    \begin{itemize}
        \item Perturbation-based ESC
        \item Model-based ESC
        \item Numerical optimization-based ESC
        \item \alert<2>{Sliding-mode-based ESC (SM-ESC)}
    \end{itemize}
\end{textblock}
    \only<2>{
    \begin{tikzpicture}[remember picture, overlay]
        \coordinate [shift={(-0.2\paperwidth,2.8cm)}] (origin) at (current page.south);
        \draw [->, line width=1pt, rounded corners=5pt] (origin) -- ++(0,-1.5cm) -- ++(1cm,0)
            node [anchor=west] {\alert{periodic switching functions}};
        \draw [->, line width=1pt] (origin) -- ++(0,-0.75cm) -- ++(1cm,0)
            node [anchor=west] {monitoring functions};
        \draw [line width=1pt] (origin) +(-0.25,0) -- +(0.25,0);
    \end{tikzpicture}}
\end{frame}

%%%%

\begin{frame}{Literature Review \,---\, Sliding-Mode-Based ESC (SM-ESC)}
    The periodic switching function approach to extremum-seeking
    %
    \setlength{\abovedisplayskip}{0pt}
    \begin{subequations}
    \label{eq:review.drakunov}
    \begin{align}
        u(t) &= \rho \sign\left( \sin\left[ \frac{\pi \sigma(t)}{T} \right] \right)
        \\
        \sigma(t) &= y(t) + \lambda \int_0^t [y(\tau) - \bar y] d\tau
            \,\, , \,\,\,\, \bar y \leq y^*
    \end{align}
    \end{subequations}
    %
    was proposed by Drakunov and {\"O}zg{\"u}ner (1992). \visible<2->{Forcing a
    time-scale separation, as it is done in perturbation-based ESC, other
    authors (Haskara et al. 2000; Yu et al. 2002a) show
    that~\eqref{eq:review.drakunov} is applicable to more general exponentially
    stable nonlinear systems.}
\end{frame}

\begin{frame}{Literature Review \,---\, Sliding-Mode-Based ESC (SM-ESC)}
\begin{textblock}{0.9}(0.05,0.18)
    It was only more than a decade later that Oliveira et al. (2010b) and
    Oliveira et al. (2011) generalized control~\eqref{eq:review.drakunov} to cover
    strongly nonlinear systems, without resorting to singular perturbations:

    \centering
    \begin{tikzpicture}[align=center]
        \setlength{\abovedisplayskip}{0pt}
        \setlength{\belowdisplayskip}{0pt}
        \node [text width=0.7\textwidth] (drakunov) {
            \begin{minipage}{\textwidth}
            \begin{subequations}
            \begin{flalign*}
            u(t) &= \rho \sign\left( \sin\left[ \frac{\pi \sigma(t)}{T} \right] \right) &
            \\
            \sigma(t) &= y(t) + \lambda \int_0^t [y(\tau) - \bar y] d\tau &
            \end{flalign*}
            \end{subequations}
            \end{minipage}
        };
        \node [text width=0.7\textwidth, below=1.5em] (oliveira) at (drakunov.south) {
            \begin{minipage}{\textwidth}
            \begin{subequations}
            \begin{flalign}
            u(t) &= \alert{\rho(t)} \sign\left( \sin\left[ \frac{\pi \sigma(t)}{T} \right] \right) &
            \\
            \sigma(t) &= y(t) + \lambda \int_0^t \alert{\sign}[y(\tau) - \bar y] d\tau &
            \end{flalign}
            \end{subequations}
            \end{minipage}
        };
        \path (drakunov.west) -- (oliveira.west) coordinate [pos=0.5] (mid);
        \draw [->, line width=1pt] (drakunov.west) ++(-0.3cm,0) arc (180-60:180+60:1.7cm)
            coordinate [pos=0] (start)
            coordinate [pos=1] (stop);
        \draw [line width=1pt] (start) +(0,0.25) -- +(0,-0.25);
    \end{tikzpicture}
\end{textblock}
\end{frame}

\begin{frame}{Literature Review \,---\, Open Problems}
    Considering the state-of-the-art of SM-ESC, we believe that this theory
    still needs to evolve to embrace:
    \begin{itemize}
        \item \alert{Nash equilibrium seeking for wider classes of multi-objective optimization problems}
        \item \alert{Pareto efficiency seeking for wider classes of multi-objective optimization problems}
        \item<2-> Optimal control via minimization of control Lyapunov functions, similar to (Scheinker et al. 2016)
        \item<3-> \alert<3>{Practice-oriented control laws}
    \end{itemize}
\end{frame}