\section{Tracking Under Unknown Control Direction}

\begin{frame}{SISO Input-Affine \,---\, Problem Formulation}
\begin{textblock}{0.88}(0.06,0.2)
    Consider single-input single-output input affine nonlinear systems%
    %
    \begin{subequations}
    \label{eq:smc.model.siso}
    \begin{align}
        \label{eq:smc.model.siso.state}
        \dot{x}(t) &= f(x) + g(x) u(t)
        \\
        \label{eq:smc.model.siso.out}
        y(t) &= h(x)
    \end{align}
    \end{subequations}
    %
    with time $t \in \realsE \geq 0$, state variable $x(t) \in \reals^n$, state and
    input functions $f,g: \reals^n \mapsto \reals$, output $y(t) \in \reals$ and
    output function $h : \reals^n \mapsto \reals$.
\end{textblock}

    \begin{tikzpicture}[remember picture, overlay]
        \node<2-> [anchor=south, above=0.08\paperheight] at (current page.south) {
            \includegraphics[width=0.9\paperwidth]{img/latex/bd_control_siso}
        };
    \end{tikzpicture}

\end{frame}

\begin{frame}{SISO Input-Affine \,---\, Output Tracking}
    Suppose system~\eqref{eq:smc.model.siso} is required to follow a known
    reference
    %
    \begin{align}
        y_m(t),\dot y_m(t) \in \reals
    \end{align}
    %
    with pre-specified velocity $\dot y_m(t)$.
    %
    Define the tracking error
    %
    \begin{subequations}
    \label{eq:error.tracking.siso}
    \begin{align}
        e(t) &= y(t) - y_m(t)
        \\
        \dot{e}(t) &= L_f h(x) + \alert<2>{k_p(x) u(t)} - \dot y_m(t)
    \end{align}
    \end{subequations}
    %
    where \alert<2>{$u$} is the control input yet to be specified.
\end{frame}

\begin{frame}{SISO Input-Affine \,---\, Problem Formulation}
    \setlength{\abovedisplayskip}{2pt}
    \setlength{\belowdisplayskip}{2pt}
    \begin{assumption}[Unit Relative Degree]
        System~\eqref{eq:smc.model.siso} is assumed to have \alert{relative degree $r = 1$}, such that
        %
        \begin{align}
            k_p(x) = L_g h(x) = {\left( \frac{\partial h}{\partial x} \right)}^\mathsf{T} \! g(x)
        \end{align}
        %
        holds for every $x \in \reals^n$.
        \label{ass:unit.relative.degree}
    \end{assumption}
    \begin{assumption}[Bounded HFG]
        The system high-frequency gain is continuous $\forall x \in \reals^n $ and has a
        \alert{lower bound $\underbar{k}_p > 0$}, such that
        %
        \
        \begin{align}
            0 < \underbar{k}_p \leq \abs{k_p(x)}
        \end{align}
        %
        holds uniformly on $x$.
    \end{assumption}
\end{frame}

{
\setimagebg{img/latex/exclamation}
\begin{frame}{SISO Input-Affine \,---\, Output Tracking}
\begin{textblock}{0.88}(0.06,0.33)
    The basic idea, which was first published in (Drakunov et al. 1995) and
    later generalized by Oliveira et al. (2011), is to define a sliding variable
    $\sigma(t,e)$, such that when $\sigma$ slides and $\dot{\sigma}|_\mathrm{eq}
    = 0$, the error is driven towards zero $e(t) \to 0$.
\end{textblock}

\begin{tikzpicture}[remember picture, overlay]
    \node<2-> [anchor=south, above=0.12\paperheight] at (current page.south) {
        \includegraphics[width=0.92\paperwidth]{img/latex/bd_control_siso}
    };
\end{tikzpicture}
\end{frame}
}

\begin{frame}{SISO Input-Affine \,---\, Output Tracking}
    \setlength{\abovedisplayskip}{2pt}
    \setlength{\belowdisplayskip}{2pt}
    \vspace{-1.4em}
    \begin{block}{Definition (Sliding-Variable $\sigma$)}
        From the error dynamics~\eqref{eq:error.tracking.siso}, define the
        sliding variable
        %
        \begin{subequations}
        \label{eq:sigma.tracking.siso}
        \begin{align}
            \sigma(t) &= e(t) + \int_0^t \!\! f_e(e) d\tau
            \\
            \dot \sigma(t) &= \dot e(t) + f_e(e)
        \end{align}
        \end{subequations}
        %
        with properly chosen $f_e : \reals \mapsto \reals$.
    \end{block}
\visible<3->{
    \begin{assumption}[Stable Error Dynamics Design]
        Function $f_e : \reals \mapsto \reals$ is chosen such that
        \begin{itemize}\justifying\fontsize{11pt}{11pt}\selectfont
            \item the origin of $\dot e + f_e(e) = 0$ is globally asymptotically
            stable;
            \item solutions to $\dot e + f_e(e + \order \epsilon)$ are globally
            ultimately bounded, with ultimate bound also of order $\order
            \epsilon$.
        \end{itemize}
        \label{ass:sigma.fe}
    \end{assumption}
}
\visible<2->{
    \begin{tikzpicture}[remember picture, overlay]
        \draw [<-, line width=1pt] (current page.center) ++(1cm,1.2cm) -- ++(1.4cm,-1.1cm)
            node [pos=1, anchor=west] {$=\lambda\sign(e)$}
            node [pos=0.55, anchor=south, sloped, yshift=-3pt, font=\fontsize{8pt}{8pt}\selectfont] {previously};
    \end{tikzpicture}
}
\end{frame}

\begin{frame}{SISO Input-Affine \,---\, Output Tracking}
    \setlength{\abovedisplayskip}{6pt}
    \setlength{\belowdisplayskip}{4pt}
    \vspace{-2ex}
    Using the sliding variable $\sigma$ definition and the error dynamics,
    the $\sigma$-dynamics is written in input disturbance form
    %
    \begin{subequations}
    \label{eq:sigma.tracking.siso.input.disturbance}
    \begin{align}
        \dot \sigma(t) &= k_p(x) \left[
            u(t) + d_\sigma(t)
        \right]
        \\
        d_\sigma(t) &= \left[
            L_f h(x) + f_e(e) - \dot y_m(t)
        \right] / k_p(x)
    \end{align}
    \end{subequations}
    %
    \vspace{-2ex}
\visible<2->{
    \begin{assumption}[Disturbance Boundedness]
        There exist known functions $\alpha_1 \in \classK$ and $\beta_1 \in \classKL$ such that
        $\norm{L_f h(x)} \leq \alpha_1( \norm{x} ) + \beta_1(\norm{x(0)}, t)$ and
        %
        \begin{align}
        \label{eq:bound.disturbance.sigma.state}
            \abs{d_\sigma} \leq \frac{1}{\underbar k_p} \big[
                \alpha_1(\!\alert<3>{\norm{x}}) + \abs{f_e(e) - \dot y_m(t)} + \beta_1(\norm{x(0)}, t)
            \big]
        \end{align}
        \label{ass:dist.bounded}
    \end{assumption}
}
\end{frame}

{
%%%% TODO %%%%
\setimagebg{img/latex/exclamation}
\begin{frame}{SISO Input-Affine \,---\, Output Tracking}
\begin{textblock}{0.88}(0.06,0.25)
    Finally, with the bound $\bar d_\sigma$, the controller that ensures
    (practical) tracking can be presented.
    %
    The control law consists of two stages:
    %
    \begin{enumerate}\justifying
        \item Despite the matched disturbance $d_\sigma$, \alert{$\sigma$
        reaches a real sliding-mode at $\sigma = k T$}, for some integer $k \in
        \integers$;
        %
        \item After $\sigma$ slides, the \alert{tracking error is driven to
        zero}.
    \end{enumerate}
\end{textblock}
    \begin{tikzpicture}[remember picture, overlay]
    \node [anchor=south, above=0.12\paperheight] at (current page.south) {
        \includegraphics[width=0.9\paperwidth]{img/latex/bd_control_siso}
    };
\end{tikzpicture}
\end{frame}
}

{
\nofootline
\begin{frame}{SISO Input-Affine \,---\, Output Tracking}
\begin{textblock}{0.94}(0.03,0.15)
    \begin{result}[SISO Sliding-Mode Controller Design]
        Selecting the control law
        \begin{subequations}
        \label{eq:control.siso}
        \begin{align}
            \label{eq:control.siso.sigmoid}
            \alert{u(t)} &\alert{= \rho(\eta, e, \dot y_m) \sigmoid{}_{\!\epsilon}\!
                \left(\sin
                    \left[
                        \frac{\pi}{T}\sigma(t)
                    \right]
                \right)}
            \\
            \label{eq:control.siso.modulation}
            \rho(\eta, e, \dot y_m) &= \frac{\kappa}{\underbar k_p} \left[
                \bar{d}_\sigma(\eta, e, \dot y_m) + \delta
            \right]
        \end{align}
        \end{subequations}
        %
        we show that, for
        system~\eqref{eq:sigma.tracking.siso.input.disturbance}, \alert{(i) no
        finite-time escape} occurs in the system signals, \alert{(ii) $\sigma$
        achieves an $\order{\epsilon}$ real sliding-mode on the sliding manifold
        $\sigma = k T$} in finite-time, and \alert{(iii) the output tracking
        error origin is uniformly globally practical asymptotically stable}
        (UGPAS), with ultimate bound $\abs{e}
        \leq \order{\epsilon}$.
        \label{prop:smc.siso}%
    \end{result}
\end{textblock}
\end{frame}
}

%%%%

{
\setimagebg{img/latex/exclamation}
\begin{frame}{SISO Input-Affine \,---\, Output Tracking Example}
\centering\Large
Example
\\[1ex]
Speed control of a Van der Pol oscillator with\\[-0.5ex]\alert{velocity measurement}
\end{frame}
}

{
\nofootline
\begin{frame}{SISO Input-Affine \,---\, Output Tracking Example}
\setlength{\abovedisplayskip}{8pt}
\setlength{\belowdisplayskip}{8pt}
\begin{textblock}{0.27}(0.69,0.17)
\fontsize{9pt}{10pt}\selectfont
Oscillator model:
%
\begin{gather*}
    m \ddot y + b(y) \dot y + k y = u
    \\
    b(y) = b_1[(y/b_0)^2 - 1]
\end{gather*}
%
with nonlinear damping $b(y)$, and known bounds
%
\begin{align*}
    b(y) &\leq \bar b(y)
    \\
    k &\leq \bar k
\end{align*}
%
such that the modulation
%
\begin{align*}
    \rho(t) = \,& \kappa ( \abs{\bar k y + \bar b(y)\dot y} + \\
        & \abs{f_e(e) - \dot y'_m(t)} + \delta )
\end{align*}
%
Simulations are performed with both $f_e(.) = \lambda \sign(.)$ and
$f_e(.)=\lambda\sat_{\bar\epsilon}(.)$.
\end{textblock}
    \begin{tikzpicture}[remember picture, overlay]
        \node [yshift=-0.055\paperheight, anchor=west] at (current page.west)
            {\includegraphics[height=0.87\paperheight]{text/figs/sim/sim_tracking_siso}};
    \end{tikzpicture}
\end{frame}
}

{
\nofootline
\begin{frame}{SISO Input-Affine \,---\, Output Tracking Example}
\begin{textblock}{0.9}(0.05,0.17)
    To display the controller ability to track changes to the HFG, let the input
    be pre-multiplied by $\gamma(t) = \sign[\sin(2\pi t/0.6)]$, such that it changes sign
    every 0.3 seconds.
\end{textblock}
    %
    \begin{tikzpicture}[remember picture, overlay]
        \node [yshift=0.02\paperheight, anchor=south] at (current page.south)
            {\includegraphics[height=0.6\paperheight]{text/figs/sim/sim_tracking_siso_hfg_change}};
    \end{tikzpicture}
\end{frame}
}

{
\nofootline
\begin{frame}{SISO Input-Affine \,---\, Output Tracking Example}
\begin{textblock}{0.9}(0.05,0.15)

    Without resorting to high-frequency switching, the proposed controller is
    able to robustly track the output. In fact, for different modulation
    function choices ($\rho_1$ computed from the bounds, $\rho_2=40$ and $\rho_3
    = 100$), the control converges to the equivalent control.
\end{textblock}
    %
    \begin{tikzpicture}[remember picture, overlay]
        \node [yshift=0.012\paperheight, anchor=south] at (current page.south)
            {\includegraphics[height=0.6\paperheight]{text/figs/sim/sim_tracking_siso_controls_sigm}};
    \end{tikzpicture}
\end{frame}
}

%%%%

{
\setimagebg{img/latex/exclamation}
\begin{frame}{SISO Input-Affine \,---\, Arbitrary Relative Degree}
    Advantages:
    %
    \begin{itemize}
        \item Does not require any extra filters or knowledge of a normal form to implement;
        \item Takes advantage of an existing time-scale separation.
        \item Needs fewer parameters to tune.
    \end{itemize}
    %
    Disadvantages:
    \begin{itemize}
        \item Necessarily relies on a time-scale separation between the controller and the plant dynamics.
    \end{itemize}
\end{frame}
}

{
\setimagebg{img/latex/exclamation}
\begin{frame}{SISO Input-Affine \,---\, Arbitrary Relative Degree Example}
\centering\Large
Example
\\[1ex]
Speed control of a Van der Pol oscillator with\\[-0.5ex]\alert{position measurement}
\end{frame}
}

{
\nofootline
\begin{frame}{SISO Input-Affine \,---\, Arbitrary Relative Degree Example}
\begin{textblock}{0.285}(0.69,0.18)
\fontsize{9pt}{10pt}\selectfont
Once again, consider the oscillator dynamics:
%
\begin{gather*}
    m \ddot y + b(y) \dot y + k y = u
    \\
    b(y) = b_1[(y/b_0)^2 - 1]
\end{gather*}
%
only this time, an estimate $z$ for $\dot y$ is obtained from the lead filter
%
\begin{align*}
    z(t) = \frac{s}{s+\mu} \, y(t)
\end{align*}

We repeat the simulation for the same control parameters as before, and filter
coefficient $\mu = 0.005$.
\end{textblock}
%
\begin{tikzpicture}[remember picture, overlay]
    \node [yshift=-0.055\paperheight, anchor=west] at (current page.west)
        {\includegraphics[height=0.87\paperheight]{text/figs/sim/sim_tracking_fast_compare}};
\end{tikzpicture}
\end{frame}
}

{
\nofootline
\begin{frame}{SISO Input-Affine \,---\, Arbitrary Relative Degree Example}
\begin{textblock}{0.3}(0.675,0.2)
\fontsize{9pt}{10pt}\selectfont

For the chosen control parameters, $\mu > 0.005$ destabilizes the
$\sign$ function implementation.

On the other hand, the continuous implementation is \\ able to maintain a stable
behavior for $\mu \leq 0.02$.
\end{textblock}
%
\begin{tikzpicture}[remember picture, overlay]
    \node [yshift=-0.055\paperheight, anchor=west] at (current page.west)
        {\includegraphics[height=0.87\paperheight]{text/figs/sim/sim_tracking_fast}};
\end{tikzpicture}
\end{frame}
}

%%%%

\section{Single-Objective Real-Time Optimization}

%%%%

\begin{frame}{SISO Input-Affine \,---\, ESC Example}
    Recall the example given to motivate ESC via output tracking
    %
    \begin{align*}
        \dot x(t) &= u(t)
        \\
        y(t) &= h(x) = \frac{1}{2} \, [x(t)-a]^2
    \end{align*}
    %
    with $k_p(x) = x(t)-a$ changing sign at $x = a$.
    %
\visible<2->{
    Since the controller is robust to changes in the HFG sign, it is reasonable
    to assume that, by setting $y_m(t) = 1 - t$, real-time optimization is
    achieved.
}
\end{frame}

{
\nofootline
\begin{frame}{SISO Input-Affine \,---\, ESC Example}
\begin{textblock}{0.28}(0.68,0.185)
\fontsize{9pt}{10pt}\selectfont
Simple integrator example:
\begin{align*}
    \dot x(t) &= u(t)
    \\[-1ex]
    y(t) &= h(x) = [x(t)-a]^2/2
\end{align*}
%
with $k_p(x) = x(t)-a$.

Both continuous and discontinuous controllers are tested with
%
\begin{gather*}
    \rho=\lambda=5 \,\,\,\mathrm{and}\,\,\, T=1
    \\
    \epsilon = \bar\epsilon = 0.1
\end{gather*}
%
The simulation step is set to $10^{-4}$ seconds.
\end{textblock}
    \begin{tikzpicture}[remember picture, overlay]
        \node [yshift=-0.055\paperheight, anchor=west] at (current page.west)
            {\includegraphics[height=0.87\paperheight]{img/latex/sim_esc_siso}};
    \end{tikzpicture}
\end{frame}
}

%%%%

{
\setimagebg{img/latex/exclamation}
\begin{frame}{SISO Input-Affine \,---\, ESC}
\begin{textblock}{0.9}(0.05,0.35)
    This simulation illustrates what we wish to achieve with SM-ESC:
    %
    \begin{itemize}
        \item \alert{Convergence to the global minimizer} $x^* : y^* = h(x^*)$;
        \item \alert{Bounded oscillations above the minimum} $y^*$ with
        arbitrarily small amplitude of order $\order{T}$.
    \end{itemize}
    %
    To guarantee that these conditions are met, only two extra assumptions are
    needed.
\end{textblock}
\end{frame}
}

\begin{frame}{SISO Input-Affine \,---\, ESC}
\setlength{\abovedisplayskip}{2pt}
\setlength{\belowdisplayskip}{2pt}
\begin{textblock}{0.9}(0.05,0.12)
\begin{assumption}[Unique Extremum]
    The output function $h(.)$ in~\eqref{eq:smc.model.siso.out} is continuous
    and differentiable almost everywhere in its domain and have a unique
    \alert{strict global minimizer $x^*$}, with minimum $y^* = h(x^*)$.
    \label{ass:smooth.unique.maximizer}
\end{assumption}
\end{textblock}
%
\begin{textblock}{0.9}(0.05,0.45)
\begin{assumption}[Known Gradient Bound]
    For any chosen $\Delta > 0$, there exists $\bar \Delta(\Delta) > 0$ and known
    constants $L(\Delta)$ and $\underbar k_p(L)$ such that
    %
    \begin{align}
    \begin{split}
        &\alert{L \leq \norm{ \frac{\partial h(x)}{\partial x} }}
            \quad \mathrm{and} \quad
            0 < \underbar k_p(L) \leq \abs{k_p(x)} = \abs{L_g h(x)}
        \\
        &\forall x \not\in D_\Delta \hspace{-0.15em} = \hspace{-0.15em}
        \left\{
            x \in \reals^n : \norm{x - x^*} \leq \bar\Delta
            \, , \,
            \abs{y(x) - y^*} \leq \Delta
        \right\}
    \end{split}
    \end{align}
    %
    holds uniformly on $t$.
    \label{ass:bounded.output.deriv}
\end{assumption}
\end{textblock}
\end{frame}

\begin{frame}{SISO Input-Affine \,---\, ESC}
\begin{textblock}{0.9}(0.05,0.2)
\begin{result}[SISO Extremum-Seeking Control]
    Consider system~\eqref{eq:smc.model.siso} with output
    error~\eqref{eq:error.tracking.siso} based on a differentiable and
    \alert{monotonically decreasing output model} $y_m(t)$, and control law~%
    \eqref{eq:sigma.tracking.siso}, \eqref{eq:control.siso},
    %
    \begin{align}
        f_e(e) = \lambda\sigmoid{}_{\bar\epsilon}(e)
    \end{align}
    %
    with $\lambda > 0$ and $\bar\epsilon > 0$.
    %
    Then, the extremum-seeking error
    %
    \begin{align}
    \label{eq:error.esc.siso}
    e^*(t) = y(t) - y^*
    \end{align}
    is UGPAS, with ultimate bound $\abs{e^*} \leq \Delta + \big( 1 +
    \lambda/\delta \big) T$, that is \alert{$\abs{e^*} \leq \Delta +
    \order{T}$}.
    \label{thm:siso.esc}
\end{result}
\end{textblock}
\end{frame}
