\section{Multi-Objective Real-Time Optimization}

%%%%

{
\setbeamercovered{transparent}
\begin{frame}{MIMO Static \,---\, Problem Formulation}
    In this section, we finally tackle multi-objective optimization with multiple decision variables,
    %
    \begin{subequations}
    \label{eq:mimo.optim}
    \begin{align}
        \label{eq:mimo.optim:obj}
        \underset{u}{\mathrm{min}} : \,\, & \nu = J(y) = \begin{bmatrix}J_1(y) & \hdots & J{n_J}(y)\end{bmatrix}^\mathsf{T}
        \\
        \label{eq:mimo.optim:dx}
        \mathrm{s.t.} : \,\, & \uncover<1>{\dot x(t) = f(x) + g(x)u(t)}
        \\[0.5ex]
        \label{eq:mimo.optim:y}
        & y(t) = h(x)
    \end{align}
    \end{subequations}
    %
    \uncover<-3>{%
    with $n_J \geq 1$ objectives functions \alert{$J : \reals^{n_y} \mapsto \reals^{n_J}$}, with $n_y \geq 1$ outputs \alert{$h : \reals^n \mapsto \reals^{n_y}$}, and with $n \geq 1$ states and $n_u \geq 1$ inputs, such that \alert{$f : \reals^n \mapsto \reals^n$} and \alert{$g : \reals^n \mapsto \reals^{n_u}$}}.
    \begin{tikzpicture}[remember picture, overlay, align=justify]
        \node<3-> [comment box fit, anchor=center, below=2.1ex, xshift=-1em, inner xsep=1em] at (current page.center) {$n_u = n_y = n_J = \alert{n}$};
        \node<4-> [comment box fit, anchor=south, above=1.5cm] at (current page.south) {
            $\nu(x) = J \circ h(x)$ \, ``$=$'' \, $y(x) = h(x)$
        };
    \end{tikzpicture}
\end{frame}
}

%%%%

\begin{frame}{MIMO Static \,---\, Problem Formulation}
\begin{textblock}{0.9}(0.05,0.16)
    Differentiating the MIMO static model with respect to time yields
    %
    \begin{subequations}
    \begin{align}
        \dot x(t) &= u(t)
        \\
        \dot y(t) &= \frac{\partial h}{\partial x}(x) \, u(t)
    \end{align}
    \label{eq:smc.model.mimo.static.deriv}%
    \end{subequations}
    %
    For system~\eqref{eq:smc.model.mimo.static.deriv}, the HFG matrix is defined as
    %
    \begin{align}
    \label{eq:smc.hfg.mimo}
        \frac{\partial h}{\partial x}(x) = \begin{bmatrix}
            \derivp{h_1}{x_1} & \hdots & \derivp{h_1}{x_n} \\
                       \vdots & \ddots & \vdots            \\
            \derivp{h_n}{x_1} & \hdots & \derivp{h_n}{x_n}
        \end{bmatrix}
            = k_p(x) + \tilde k_p(x)
    \end{align}
    %
    where $k_p(x)$ holds the diagonal and $\tilde k_p(x)$ the off-diagonal entries of
    matrix $\partial h(x)/\partial x$.
    \begin{tikzpicture}[remember picture, overlay]
    \node<2> [comment box fit, anchor=south east, yshift=3ex, xshift=-5.5ex, inner ysep=2pt, inner xsep=4pt]
        at (current page.south east) {naturally, assumptions are made on $\partial h(x)/\partial x$};
    \end{tikzpicture}
\end{textblock}
\end{frame}

\begin{frame}{MIMO Static \,---\, Problem Formulation}
    \begin{assumption}[Unique Optimizers]
        \label{ass:mimo.unique.maximizer}
        Let $\theta_j \in \reals^{n-1}$ denote all elements of $x \in \reals^{n}$, but for the $j$-th entry, and let $h \in \classC{2}$. Then, each function  $h_j(\cdot)$ is unimodal w.r.t. $x_j$, thus, for every fixed $\theta_j \in \reals^{n-1}$ there exists a unique minimizer ${x}_j^*(\theta_j)$ such that
        %
        \begin{align}
        \left. \frac{\partial h_j}{\partial x_j} \right|_{x_j={x}_j^*}=0 \quad \mathrm{and} \quad \left.\frac{\partial^2 h_j}{\partial x_j^2} \right|_{x_j={x}_j^*} >0
        \end{align}
        %
        where $x^*_j: \reals^{n-1} \mapsto \reals$ is a continuous function of the $n-1$ $\theta_j$.
    \end{assumption}
\end{frame}

\begin{frame}{MIMO Static \,---\, Problem Formulation}
    \vspace{-4ex}
    \begin{definition}[$\Delta$-Vicinity]
        \label{def:mimo.delta.vicinity}
        The $\Delta$-vicinity of the minimizer $x_j^*(\theta_j)$ is the volume of diameter $\Delta$ along $x_j^*(\theta_j)$, i.e., the set
        %
        \vspace{-1ex}
        \begin{align}
        \mathcal{D}_{\Delta_j} = \left\{x \in \reals^n : |x_j-x_j^*(\theta_j)| \leq \frac{\Delta}{2} \right\}
        \end{align}
    \end{definition}
    \begin{tikzpicture}
        \node [anchor=center]
            {\includegraphics[width=0.92\textwidth]{img/latex/example_delta_region}};
    \end{tikzpicture}
\end{frame}

\begin{frame}{MIMO Static \,---\, Problem Formulation}
    \vspace{-2ex}
    \begin{assumption}[Nash Equilibrium Points]
        Let $\Theta^*_j$ denote the nonempty set of minimizers
        %
        \begin{align}
        \Theta^*_j = \left\{
            x^* \in \reals^{n} : \frac{\partial h_j(x^*)}{\partial x_j} = 0
            \, , \,
            \frac{\partial^2 h_j(x^*)}{\partial x_j^2} < 0
        \right\} \subseteq \mathcal{D}_{\Delta_j}
        \end{align}
        %
        which is the same as the set $\mathcal{D}_{\Delta_j}$ with $\Delta = 0$. We say that $y_j^*=h_j(x^*)$ is an extremum (minimum) of the smooth mapping $h_j(\cdot)$ when $x^* \in \Theta_j^*$. Moreover, we say that $x^*$ is a minimizing point of $h(\cdot)$ when $x^*$ is sufficiently close to
        %
        \begin{align}
        \Theta^*  =  \bigcap_{j=1}^n \Theta_j^* \neq \emptyset
        \end{align}
    \end{assumption}
\end{frame}

\begin{frame}{MIMO Static \,---\, Problem Formulation}
    \vspace{-4ex}
    \begin{assumption}[Unit Diagonal Relative Degree]
    System~\eqref{eq:smc.model.mimo.static.deriv} with HFG
    matrix~\eqref{eq:smc.hfg.mimo} is said to have unit diagonal relative
    degree, such that
    %
    \begin{align}
        \left[ k_p(x) \right]_{ii} = k_{pi}(x) = \derivp{h_i}{x_i}(x) \neq 0
            \ ,\ \forall i \in [1,n]
    \end{align}
    %
    holds for every $x \not\in \mathcal{D}_{\Delta_j}$.
    \label{ass:mimo.unit.relative.degree}
    \end{assumption}
    %
    \pause
    %
    Assuming unit diagonal relative degree and $x \not\in \mathcal{D}_{\Delta_j}$, it is possible to rewrite the process dynamics as

    \begin{tikzpicture}
        \setlength{\abovedisplayskip}{0pt}
        \setlength{\belowdisplayskip}{0pt}
        \node [anchor=center, text width=\textwidth] (eq) {%
            \begin{subequations}
            \begin{flalign}
                \label{eq:ydynamicsU}
                \dot y(t) = k_p(x) \left[
                    u(t) + k_p^{-1}(x) \tilde{k}_p(x) u(t)
                \right]&&
            \end{flalign}
            \visible<4->{
            \begin{flalign}
                \label{eq:ydynamicsV}
                \dot y(t) = \alert<5>{k_p(x) S(x)} \left[
                    v(t) + \alert<5>{S^{-1}(x) k_p^{-1}(x) \tilde{k}_p(x) S(x)} v(t)
                \right]&&
            \end{flalign}
            }
            \end{subequations}
        };
        \node<3-> [comment box fit, anchor=north east, align=center, yshift=-1ex, xshift=-5em] at (eq.north east) {$u = S(x)v$};
    \end{tikzpicture}
\end{frame}

\begin{frame}{MIMO Static \,---\, Problem Formulation}
    \vspace{-4ex}
    \begin{assumption}[Bounded High-Frequency Gain Diagonal]
        \label{ass:kpj.sj.bound}
        Outside the $\Delta$-vicinity, there exists a known constant $L_{j}(\Delta)\!>\!0$,
        %
        \begin{align}
            L_j \leq \left| k_{pj}(x) s_j(x) \right|
                \,, \quad \forall x \notin \mathcal{D}_{\Delta_j}
        \end{align}
        %
        where $\Delta$ can be made arbitrary small by allowing a smaller $L_{j}(\Delta)$.
    \end{assumption}
    \pause
    \begin{assumption}[Bounded Matched Disturbances]
        \label{ass:matched.disturbances.premult.bound}
        There exists a known state-dependent diagonal matrix $S(x) \in \reals^{n \times n}$,
        %
        \begin{align}
        \label{eq:matched.disturbance.premult}
            \left|
                S^{-1}(x) k_p^{-1}(x) \tilde{k}_p(x) S(x)
            \right| \leq P
            \ ,\ \ \forall x \notin \mathcal{D}_{\Delta_j}
        \end{align}
        %
        where $P$ is a known constant matrix with non-negative elements. The inequality is understood element-wise and all entries of the main diagonal of $S(x)$ are continuous positive functions $s_{j}: \reals^{n} \mapsto \reals^+$.
        %
    \end{assumption}
\end{frame}

\begin{frame}{MIMO Static \,---\, Extremum-Seeking Controller}
    \begin{tikzpicture}[remember picture, overlay]
        \node [anchor=south, above=3ex] (image siso) at (current page.center) {
            \includegraphics[width=0.92\paperwidth]{img/latex/bd_control_siso}
        };
        \node [anchor=north, below=5ex] (image mimo) at (image siso.south) {
            \includegraphics[width=0.92\paperwidth]{text/figs/tikz/bd_esc}
        };
        \node [comment box fit, anchor=north west] at (image siso.north west) {
            \small SISO
        };
        \node [comment box fit, anchor=north west, yshift=1ex] at (image mimo.north west) {
            \small MIMO
        };

        \draw [\cnDarkGrey, dashed] ($(image siso.south west) - (0, 2ex)$) -- ($(image siso.south east) - (0, 2ex)$);
    \end{tikzpicture}
\end{frame}

%%%%

\begin{frame}{MIMO Static \,---\, Stability and Convergence Results}
    \begin{textblock}{0.9}(0.05,0.12)
    \begin{result}[Nash Equilibrium Seeking Controller]
        Consider the system~\eqref{eq:smc.model.mimo.static.deriv}, with control law as shown in the figure below. Then, (i) sliding-modes on $\sigma_j = k^*T_j$ are achieved in some finite-time $t_j < t_0 + \order{T_j/\delta_j}$, (ii) all $\Delta_j$-vicinities are globally attractive and achieved in finite-time, (iii) for sufficiently small design constants, the oscillations above the minimum $y^*$ of $y$ can be made of order $\order{\max_j T_j}$, (iv) and no finite-time escape occurs in the close-loop signals.
    \end{result}
    \begin{tikzpicture}[remember picture, overlay]
        \node [anchor=south, above=.618ex] at (current page.south) {
            \includegraphics[height=0.38\paperheight]{text/figs/tikz/bd_esc}
        };
    \end{tikzpicture}
    \end{textblock}
\end{frame}

%%%%

{
\setimagebg{img/latex/exclamation}
\begin{frame}{MIMO Static \,---\, Raman Optical Amplifiers}
\centering\Large
Example
\\[1ex]
Optimization of the output signal power spectrum of Raman optical amplifiers
\end{frame}
}

{
\nofootline
\begin{frame}{MIMO Static \,----\, Raman Optical Amplifiers}
\begin{tikzpicture}[remember picture, overlay]
    \only<1>{
        \node [anchor=east] (cost 1) at (current page.center) {
            \includegraphics[width=0.48\paperwidth]{text/figs/raman/objective_surf_1}
        };
        \node [anchor=west] (cost 2) at (current page.center) {
            \includegraphics[width=0.48\paperwidth]{text/figs/raman/objective_surf_2}
        };
        \node [anchor=north] at (cost 1.south) {(a) Nominal static map $h_1$};
        \node [anchor=north] at (cost 2.south) {(b) Nominal static map $h_2$};}
    \only<2>{
        \node [anchor=south, yshift=0.2ex] at (current page.south) {
            \includegraphics[height=0.87\paperheight]{text/figs/raman/downstream_power_spectrum}
        };
    }
    \only<3>{
        \node [anchor=south, yshift=2ex] at (current page.south) {
            \includegraphics[height=0.8\paperheight]{text/figs/raman/phase_portrait_disturbances}
        };
    }
\end{tikzpicture}
\end{frame}
}

{
\setimagebg{img/latex/exclamation}
\begin{frame}{Multi-Objective Real-Time Optimization}
    We do develop \alert{other results on multi-objective optimization} throughout the thesis. For shortness, however, these results are omitted in this presentation. They are:
    \begin{itemize}\justifying
        \item the \alert{generalization} of the previous controller for optimization problems subject to the dynamics of an \alert{input affine nonlinear} process, and
        \item an algorithm that can be used for \alert{Pareto efficiency seeking in multi-objective problems though scalarization}.
    \end{itemize}
\end{frame}
}