# Configure
```
git submodule sync
git submodule foreach git reset --hard
git submodule foreach git pull
```

# Compile
```
make
```
