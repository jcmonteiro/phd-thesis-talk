\graphicspath{{img/}}

\usetheme{sthlm}

% For textblock
\usepackage[absolute,overlay]{textpos}
\setlength{\TPHorizModule}{\paperwidth}
\setlength{\TPVertModule}{\paperheight}

% For drawing
\usepackage{tikz}
\tikzstyle{comment box fit}=[
    rounded corners=2pt,
    rectangle,
    fill=sthlmLightYellow,
    text=sthlmDarkGrey,
    draw=black,
    inner sep=1.1ex]
\tikzstyle{comment box}[\linewidth]=[
    comment box fit,
    text width=#1]
\tikzset{>=latex}

% Standard packages
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{psfrag}
\usepackage{amsmath}
\usepackage{pifont}
\usepackage{color}
\usepackage{multicol}
\usepackage{vwcol}
\usepackage{dsfont}
\usepackage{ragged2e} % \justifying

\usepackage{graphicx} % for pdf, bitmapped graphics files

\usepackage[
    style=alphabetic,
    citestyle=authortitle,
    maxcitenames=1,
    sorting=nyt,
    sortcites=true,
    autopunct=true,
    babel=hyphen,
    hyperref=true,
    abbreviate=false,
    backref=true,
    backend=biber,
    natbib=true
]{biblatex}
%\addbibresource{../paper.bib} % BibTeX bibliography file

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title[Sliding-Mode Control --- Tracking and Optimization]{%
    \fontsize{19}{19}\selectfont%
    Tracking and Optimization of Uncertain Nonlinear Systems with Unknown Control Direction via Sliding-Mode Control
}

\author[Jo{\~a}o C. Monteiro]{\small{%
    D.Sc. Student: Jo{\~a}o Carlos Monteiro \\
    Adviser: Alessandro Jacoud Peixoto
}}

\authorpicture{photo}
    
\institute[UFRJ]{%
    Dept. of Electrical Engineering \\
    Federal University of Rio de Janeiro \\
    Rio de Janeiro, Brazil \\
} 

\date{March, 27th 2020}

\conference{D.Sc. Qualifying Exam}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\setimagebg}[1]{%
\usebackgroundtemplate{%
  \parbox[c][\paperheight][c]{\paperwidth}{\centering\includegraphics[height=\paperheight]{#1}}%
}%
}

\newcommand{\setimagebgw}[1]{%
\usebackgroundtemplate{%
  \parbox[c][\paperheight][c]{\paperwidth}{\centering\includegraphics[width=\paperwidth]{#1}}%
}%
}

\newcommand{\imagetext}[2][white]{{\usebeamerfont{subtitle}\bfseries\color{#1}#2}}

\newcommand{\imageabove}[2]{%
\begin{tikzpicture}[remember picture, overlay]%
    \fill [black, opacity=0.5] (current page.north west) rectangle (current page.south east);%
    \node at (current page.center) {\includegraphics[width=#1]{#2}};%
\end{tikzpicture}%
}

\definecolor{color_highlight}{RGB}{200, 230, 201}
\definecolor{color_working}{RGB}{255, 249, 196}
\newcommand{\worksymbol}[1]{\raisebox{-.5ex}{\begin{tikzpicture}\draw[fill=#1,draw=black,line width=1pt] (0,0) circle (1ex);\end{tikzpicture}}}
\newcommand{\workgreen}{\worksymbol{color_highlight}}
\newcommand{\workyellow}{\worksymbol{color_working}}
\newcommand{\workatob}[2]{\worksymbol{#1} \color{black}$\to$ \worksymbol{#2}}

\newcommand{\imagecalculating}{\imageabove{0.3\paperwidth}{tedious}}

\newcommand{\nofootline}{\setbeamertemplate{footline}{}}

% Custom commands
\DeclareMathOperator*{\sign}{sign}
\DeclareMathOperator*{\sat}{sat}
\DeclareMathOperator*{\sigmoid}{sigmoid}
\DeclareMathOperator*{\diag}{diag}
\DeclareMathOperator*{\signeps}{\sigmoid_{\displaystyle\epsilon}}

\providecommand*{\floor}[1]{\left\lfloor#1\right\rfloor}
\providecommand*{\mathset}[1]{\mathbb{#1}}
\providecommand*{\naturals}{\mathset{N}}
\providecommand*{\reals}{\mathset{R}}
\providecommand*{\preals}{\reals_+}
\providecommand*{\realsE}{\bar{\reals}}
\providecommand*{\prealsE}{\bar{\reals}_+}
\providecommand*{\integers}{\mathset{Z}}
%
\providecommand*{\classC}[1]{C^{#1}}
\providecommand*{\classK}{\mathcal{K}}
\providecommand*{\classKL}{\classK\mathcal{L}}
\providecommand*{\continuous}{\classC{0}}
\providecommand*{\vecvar}[1]{#1}
\providecommand*{\abs}[1]{\left|#1\right|}
\providecommand*{\norm}[1]{\left| \! \left| #1 \right| \! \right|}
\providecommand*{\order}[1]{\mathcal{O}(#1)}
\providecommand{\transp}[1]{{#1}^\mathsf{T}}
\providecommand*{\interval}{\mathcal{I}}
\providecommand*{\inner}[2]{< \! #1,#2 \! >}
\providecommand*{\ball}[1]{B(#1)}
\providecommand*{\contingent}[2]{D_{\mathbb{#1}}#2}
\providecommand*{\dcontingent}[4]{D_{\mathbb{#1},\mathbb{#2}(#3)}#4}
\providecommand*{\citep}[1]{\parencite{#1}}
\providecommand*{\derivp}[2]{\dfrac{\partial #1}{\partial #2}}

\providecommand*{\graph}{\mathcal{G}}
\providecommand*{\setlabels}{\mathcal{V}}
\providecommand*{\setedges}{\mathcal{E}}
\providecommand*{\iff}{\Longleftrightarrow}
\providecommand*{\setinputs}{\mathcal{U}}
\providecommand*{\setestimates}{\mathcal{X}}
\providecommand*{\diamgraph}[1][\graph]{d\!\left(#1\right)}

\setbeamertemplate{theorem}[ams style]
\setbeamertemplate{theorems}[numbered]

% Taken from https://tex.stackexchange.com/questions/82415/beamer-different-numbering-for-theorems-examples-definition-and-lemma
\makeatletter
    \ifbeamer@countsect
      \newtheorem{theorem}{\translate{Theorem}}[section]
    \else
      \newtheorem{theorem}{\translate{Theorem}}
    \fi
    \newtheorem{corollary}{\translate{Corollary}}
    \newtheorem{fact}{\translate{Fact}}
    \newtheorem{lemma}{\translate{Lemma}}
    \newtheorem{problem}{\translate{Problem}}
    \newtheorem{solution}{\translate{Solution}}
    \newtheorem{assumption}{\translate{Assumption}}
    \newtheorem{proposition}{\translate{Proposition}}
    \newtheorem{conjecture}{\translate{Conjecture}}
    \newtheorem{result}{\translate{Result}}

    \theoremstyle{definition}
    \newtheorem{definition}{\translate{Definition}}
    \newtheorem{definitions}{\translate{Definitions}}

    \theoremstyle{example}
    \newtheorem{example}{\translate{Example}}
    \newtheorem{examples}{\translate{Examples}}
\makeatother